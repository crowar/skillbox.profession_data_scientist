import re

workPath = '.\\Основы Python (2019)\\07. исключения\\'

class NotNameError(ValueError):
    def __init__(self, message):            
        super().__init__(message)

class NotEmailError(ValueError):
    def __init__(self, message):            
        super().__init__(message)

class Validator:
    def Validate(line):
        splitedLine = line.split(' ')

        if len(splitedLine) != 3:
            raise ValueError('Ошибка в количестве параметров')

        if not(re.match('(?i)^[a-zа-яё]*$', splitedLine[0])):
            raise NotNameError('Не верно указано имя')
            
        if not(re.match("(?i)[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", splitedLine[1])):
            raise NotEmailError('Не верно указана электропочта')

        age = int(splitedLine[2])

        if age < 10 or age > 99:
            raise ValueError('Возрост не находится в допустимом значении')

class FileReader:
    def __init__(self, filePath):
        self.__filePath = filePath

    def getLines(self):
        file = None
        try:
            with open(self.__filePath, 'r', encoding='utf8') as file:
                for line in file:
                    if line.strip() != "":
                        yield line.strip()
        # except IOError:
        except FileNotFoundError:
            print('Файл "%s" не найден' % self.__filePath)
        finally:
            if file is not None:
                file.close()

class FileWriter:
    def  __init__(self, filePath):
        self.__filePath = filePath
        self.__file = open(self.__filePath, 'w', encoding='utf8')

    def __del__(self):
        if self.__file is not None:
            self.__file.close()

    def writeLine(self, line):
        self.__file.write(line + '\n')

class Worker:
    def analyze(fileName):
        fReader = FileReader(workPath + fileName)
        # fReader = FileReader(workPath + 'test.txt')

        try:
            infoLog = FileWriter(workPath + 'registrations_good.log')
            errorLog = FileWriter(workPath + 'registrations_bad.log')

            goodRow = 0
            badRow = 0
            allRow = 0

            for line in fReader.getLines():

                allRow += 1

                try:
                    Validator.Validate(line)
                except (ValueError) as ex:
                    badRow += 1
                    errorLog.writeLine('При обработке строки "%s" возникла ошибка %s(%s)' % (line, type(ex).__name__, ex))
                else:
                    goodRow += 1
                    infoLog.writeLine(line)
            
            print('Файл обработан:\n\tВсего записей: %d\n\tУспешных: %d\n\tОшибок: %d' % (allRow, goodRow, badRow))
        except Exception as ex:
            print(ex.args)

Worker.analyze("registrations_.txt")