import math
from types import *

class Fraction:

    def __init__(self, a: int, b: int = 1):
        self.numerator = a

        if (b == 0):
            raise ValueError('Знаменатель не может равняться нулю')
        elif (b < 0):
            raise ValueError('знаменатель должен быть больше нуля')
        else:
            self.denominator = b

        self.__rounding()

    def __rounding(self):
        divisor = math.gcd(self.numerator, self.denominator)

        self.numerator = int(self.numerator / divisor)
        self.denominator = int(self.denominator / divisor)

    def __str__(self):
        if self.numerator % self.denominator == 0:
            return('%d' % (self.numerator / self.denominator))

        return('%d/%d' % (self.numerator, self.denominator))

    def __add__(self, value):
        if isinstance(value, int):
            return Fraction(self.numerator + value * self.denominator, self.denominator)
        elif isinstance(value, Fraction):
            numerator = self.numerator * value.denominator + value.numerator * self.denominator
            denominator = self.denominator * value.denominator

            return Fraction(numerator, denominator)
        else:
            return ValueError('Значение с типом "%s" не может участвовать в сложении' % type(value).__name__)


    def __sub__(self, value):
        if isinstance(value, int):
            return Fraction(self.numerator - value * self.denominator, self.denominator)
        elif isinstance(value, Fraction):
            numerator = self.numerator * value.denominator - value.numerator * self.denominator
            denominator = self.denominator * value.denominator

            return Fraction(numerator, denominator)
        else:
            return ValueError('Значение с типом "%s" не может участвовать в сложении' % type(value).__name__)

    def __mul__(self, value):
        if isinstance(value, int):
            return self * Fraction(value*self.denominator, self.denominator)
        elif isinstance(value, Fraction):
            numerator = self.numerator * value.numerator
            denominator = self.denominator * value.denominator

            return Fraction(numerator, denominator)
        else:
            return ValueError('Значение с типом "%s" не может участвовать в сложении' % type(value).__name__)

    def __int__(self):
        return int(self.numerator // self.denominator)

    def __float__(self):
        return self.numerator / self.denominator

class OperationsOnFraction(Fraction):
    def getint(self):
        return int(self)
    
    def getfloat(self):
        return float(self)


print(math.gcd(3,9))

print("Операция: 3/9 => %s" % Fraction(3, 9))

print("Операция: 3/9 + 1 => %s" % (Fraction(3, 9) + 1))
print("Операция: 1/3 + 1/9 => %s" % (Fraction(1, 3) + Fraction(1, 9)))

print("Операция: 3/2 - 1 => %s" % (Fraction(3, 2) - 1))
print("Операция: 2/3 - 1/3 => %s" % (Fraction(2, 3) - Fraction(1, 3)))

print("Операция: 1/2 * 2 => %s" % (Fraction(1, 2) * 2))
print("Операция: 1/2 * 1/2 => %s" % (Fraction(1, 2) * Fraction(1, 2)))

print("Операция: int(1/2) => %s" % (int(Fraction(1, 2))))
print("Операция: int(2/2) => %s" % (int(Fraction(2, 2))))
print("Операция: int(3/2) => %s" % (int(Fraction(3, 2))))

print("Операция: float(1/2) => %s" % (float(Fraction(1, 2))))
print("Операция: float(2/2) => %s" % (float(Fraction(2, 2))))
print("Операция: float(3/2) => %s" % (float(Fraction(3, 2))))

print("Операция: OperationsOnFraction(3,2).getint() => %s" % OperationsOnFraction(3,2).getint())
print("Операция: OperationsOnFraction(3,2).getfloat() => %s" % OperationsOnFraction(3,2).getfloat())
